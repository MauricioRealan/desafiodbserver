package br.com.desafiodbserver.test;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.repositorio.RestauranteRepositorio;
import br.com.desafiodbserver.repositorio.VotacaoRepositorio;

public class VotacaoRepositorioTeste {

	@Ignore
	@Test
	public void testarCriarVotacao() {
		
		RestauranteRepositorio repositorio = new RestauranteRepositorio();
		ArrayList<Restaurante> lista = repositorio.criarRestaurantes();
		ArrayList<Restaurante> disponiveis = new ArrayList<Restaurante>();
		
		for(Restaurante r : lista) {
			if(r.getVisitado() == false) {
				disponiveis.add(r);
			}
		}
		
		VotacaoRepositorio votacao = new VotacaoRepositorio();
		ArrayList<Votacao> votos = new ArrayList<Votacao>();
		votos = votacao.criarVotacao(disponiveis);
		
		for(Votacao v : votos) {
			System.out.println("restaurante:" + v.getRestaurante().getNome());
			System.out.println("situacao:" + v.getRestaurante().getVisitado());
			System.out.println("numero de votos:" + v.getVotos());
		}
		
	}
	
	@Test
	public void testarAlimentarVotacao() {
	
		RestauranteRepositorio repositorio = new RestauranteRepositorio();
		ArrayList<Restaurante> lista = repositorio.criarRestaurantes();
		ArrayList<Restaurante> disponiveis = new ArrayList<Restaurante>();
		
		for(Restaurante r : lista) {
			if(r.getVisitado() == false) {
				disponiveis.add(r);
			}
		}
		
		VotacaoRepositorio votacao = new VotacaoRepositorio();
		ArrayList<Votacao> votos = new ArrayList<Votacao>();
		votos = votacao.criarVotacao(disponiveis);
		
		votos = votacao.alimentarVotacao(votos);
		
		for(Votacao v : votos) {
			System.out.println("restaurante:" + v.getRestaurante().getNome());
			System.out.println("situacao:" + v.getRestaurante().getVisitado());
			System.out.println("numero de votos:" + v.getVotos());
		}
		
	}
		
	
}
