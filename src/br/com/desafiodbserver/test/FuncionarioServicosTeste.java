package br.com.desafiodbserver.test;

import org.junit.Test;

import br.com.desafiodbserver.servicos.FuncionarioServicos;

public class FuncionarioServicosTeste {

	@Test
	public void testarChecarUsuario() {
		
		FuncionarioServicos funcionario = new FuncionarioServicos();
		
		Boolean resultadoa = funcionario.checarUsuario("Mauricio", "dbserver");
		System.out.println("Resultado: " + resultadoa);
		
		Boolean resultadoe = funcionario.checarUsuario("errado", "errado");
		System.out.println("Resultado: " + resultadoe);
		
	}
	
}
