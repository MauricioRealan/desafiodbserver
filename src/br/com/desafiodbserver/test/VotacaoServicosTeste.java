package br.com.desafiodbserver.test;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.repositorio.RestauranteRepositorio;
import br.com.desafiodbserver.servicos.VotacaoServicos;

public class VotacaoServicosTeste {

	@Ignore
	@Test
	public void testarNovoVoto() {
		
		RestauranteRepositorio repositorio = new RestauranteRepositorio();
		ArrayList<Restaurante> lista = repositorio.criarRestaurantes();
		ArrayList<Restaurante> disponiveis = new ArrayList<Restaurante>();
		
		for(Restaurante r : lista) {
			if(r.getVisitado() == false) {
				disponiveis.add(r);
			}
		}
		
		VotacaoServicos votacao = new VotacaoServicos();
		
		// case 1: primeiro voto
		ArrayList<Votacao> votos = votacao.novoVoto("Churrascaria do Jo�o", disponiveis, 1);
		
		for(Votacao v : votos) {
			System.out.println("Restaurante:" + v.getRestaurante().getNome());
			System.out.println("Situacao:" + v.getRestaurante().getVisitado());
			System.out.println("Numero de Votos:" + v.getVotos());
		}
		
		// case 2: novo voto
		ArrayList<Votacao> votosnovos = votacao.novoVoto("Churrascaria do Jo�o", disponiveis, 2);
		
		for(Votacao v : votosnovos) {
			System.out.println("Restaurante:" + v.getRestaurante().getNome());
			System.out.println("Situacao:" + v.getRestaurante().getVisitado());
			System.out.println("Numero de Votos:" + v.getVotos());
		}
		
	}
	
	@Ignore
	@Test
	public void testarBuscarResultado() {
		
		Votacao v1 = new Votacao();
		Restaurante r1 = new Restaurante();
		r1.setNome("teste1");
		r1.setVisitado(false);
		v1.setRestaurante(r1);
		v1.setVotos(5);
		Votacao v2 = new Votacao();
		Restaurante r2 = new Restaurante();
		r2.setNome("teste2");
		r2.setVisitado(false);
		v2.setRestaurante(r2);
		v2.setVotos(4);
		Votacao v3 = new Votacao();
		Restaurante r3 = new Restaurante();
		r3.setNome("teste3");
		r3.setVisitado(false);
		v3.setRestaurante(r3);
		v3.setVotos(8);
		
		ArrayList<Votacao> lista = new ArrayList<Votacao>();
		lista.add(v1);
		lista.add(v2);
		lista.add(v3);
		
		VotacaoServicos votacao = new VotacaoServicos();
		String[] resultado = new String[6];
		resultado = votacao.buscarResultado(lista);
		
		for(int i = 0; i < 6; i++) {
			System.out.println("Restaurante: " + resultado[i] + " Votos: " + resultado[i+1]);
			i = i + 2;
		}
		
	}
	
	@Test
	public void testarConferirVotantes() {
		
		VotacaoServicos votacao = new VotacaoServicos();
		
		Boolean utilizado = votacao.conferirVontantes("Mauricio");
		System.out.println("Mauricio j� votou hoje? " + utilizado);
		
		utilizado = votacao.conferirVontantes("Fulano");
		System.out.println("Fulano j� votou hoje? " + utilizado);
		
		
	}
	
}
