package br.com.desafiodbserver.test;

import java.util.ArrayList;

import org.junit.Test;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.repositorio.RestauranteRepositorio;

public class RestauranteRepositorioTeste {

	@Test
	public void testarCriarRestaurantes() {
		
		RestauranteRepositorio lista = new RestauranteRepositorio();
		
		ArrayList<Restaurante> restaurantes = lista.criarRestaurantes();
		
		for(Restaurante r: restaurantes) {
			System.out.println("Nome: " + r.getNome());
			System.out.println("J� foi visitado? " + r.getVisitado());
		}
		
	}
	
}
