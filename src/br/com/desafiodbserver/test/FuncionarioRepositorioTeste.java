package br.com.desafiodbserver.test;

import java.util.ArrayList;

import org.junit.Test;

import br.com.desafiodbserver.dominio.Funcionario;
import br.com.desafiodbserver.repositorio.FuncionarioRepositorio;

public class FuncionarioRepositorioTeste {

	@Test
	public void testeCriarUsuarios() {
		
		FuncionarioRepositorio lista = new FuncionarioRepositorio();
		
		ArrayList<Funcionario> funcionario = lista.criarUsuarios();
		
		for(Funcionario f : funcionario) {
			System.out.println("Usu�rio: " + f.getUsuario());
			System.out.println("Senha: " + f.getSenha());
		}
		
		
	}
	
}
