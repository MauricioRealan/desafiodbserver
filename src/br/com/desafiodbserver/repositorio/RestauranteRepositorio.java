package br.com.desafiodbserver.repositorio;

import java.util.ArrayList;

import br.com.desafiodbserver.dominio.Restaurante;

//classe que cria restaurantes FAKE, simulando um repositorio de restaurantes utilizados
public class RestauranteRepositorio {

	public ArrayList<Restaurante> criarRestaurantes() {

		ArrayList<Restaurante> restaurantes = new ArrayList<Restaurante>();

		Restaurante novo = new Restaurante();
		novo.setNome("Churrascaria do Jo�o");
		novo.setVisitado(false);
		restaurantes.add(novo);
		Restaurante novo2 = new Restaurante();
		novo2.setNome("Cantina da Mama");
		novo2.setVisitado(false);
		restaurantes.add(novo2);
		Restaurante novo3 = new Restaurante();
		novo3.setNome("Lancheria Nova");
		novo3.setVisitado(true); // a condicao "true" indica que o restaurante ja foi visitado essa semana
		restaurantes.add(novo3);
		Restaurante novo4 = new Restaurante();
		novo4.setNome("Restaurante da Maria");
		novo4.setVisitado(false);
		restaurantes.add(novo4);

		return restaurantes;

	}

}
