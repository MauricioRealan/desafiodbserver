package br.com.desafiodbserver.repositorio;

import java.util.ArrayList;

import br.com.desafiodbserver.dominio.Funcionario;

// classe que cria usuarios FAKE, simulando um repositorio de funcionarios da empresa
public class FuncionarioRepositorio {

	public ArrayList<Funcionario> criarUsuarios() {

		ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();

		Funcionario novo = new Funcionario();
		novo.setUsuario("Jo�o");
		novo.setSenha("1234");
		funcionarios.add(novo);
		Funcionario novo1 = new Funcionario();
		novo1.setUsuario("Paulo");
		novo1.setSenha("12abc");
		funcionarios.add(novo1);
		Funcionario novo2 = new Funcionario();
		novo2.setUsuario("Carlos");
		novo2.setSenha("875bd");
		funcionarios.add(novo2);
		Funcionario novo3 = new Funcionario();
		novo3.setUsuario("Pedro");
		novo3.setSenha("aeiou");
		funcionarios.add(novo3);
		Funcionario novo4 = new Funcionario();
		novo4.setUsuario("Mauricio");
		novo4.setSenha("dbserver");
		funcionarios.add(novo4);

		return funcionarios;

	}

}
