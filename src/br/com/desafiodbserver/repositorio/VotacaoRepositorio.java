package br.com.desafiodbserver.repositorio;

import java.util.ArrayList;
import java.util.Random;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;

// classe que cria a votacao do dia e, ainda, gera votos aleatorios
public class VotacaoRepositorio {

	private ArrayList<Votacao> votacao = new ArrayList<Votacao>();
	Random gerarvotos = new Random();

	// metodo que inicia a votacao com uma base de votos aleatorios
	public ArrayList<Votacao> criarVotacao(ArrayList<Restaurante> r) {

		for (Restaurante restaurante : r) {
			Votacao novo = new Votacao();
			novo.setRestaurante(restaurante);
			novo.setVotos(gerarvotos.nextInt(10));// votos gerados ate 10
			votacao.add(novo);
		}

		return votacao;
	}

	// metodo que alimenta a votacao, adicionando votos a base ja criada
	public ArrayList<Votacao> alimentarVotacao(ArrayList<Votacao> v) {

		votacao = v;

		for (Votacao votos : votacao) {
			votos.setVotos(votos.getVotos() + gerarvotos.nextInt(5));// votos gerados ate 5
		}

		return votacao;

	}

}
