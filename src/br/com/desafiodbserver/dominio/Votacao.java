package br.com.desafiodbserver.dominio;

//classe que define o dominio de Votacao
public class Votacao {

	private Restaurante restaurante;
	private int votos;
	
	public int getVotos() {
		return votos;
	}
	public void setVotos(int votos) {
		this.votos = votos;
	}
	public Restaurante getRestaurante() {
		return restaurante;
	}
	public void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}
	
	
}
