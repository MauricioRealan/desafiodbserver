package br.com.desafiodbserver.dominio;

//classe que define o dominio de Restaurante
public class Restaurante {

	private String nome;
	private Boolean visitado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getVisitado() {
		return visitado;
	}

	public void setVisitado(Boolean visitado) {
		this.visitado = visitado;
	}
	
}
