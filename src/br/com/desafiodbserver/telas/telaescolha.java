package br.com.desafiodbserver.telas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.repositorio.RestauranteRepositorio;
import br.com.desafiodbserver.servicos.VotacaoServicos;

import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// classe que monta a tela onde eh realizado o voto do usuario
public class telaescolha extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telaescolha frame = new telaescolha("", 0);
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public telaescolha(String nome, int numVotos) {
		setTitle("- DBServer - Sele��o Restaurantes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 434, 308);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(96, 9, 230, 143);
		label.setIcon(new ImageIcon(telaescolha.class.getResource("/br/com/desafiodbserver/telas/dbserver.png")));
		panel.add(label);

		RestauranteRepositorio repositorio = new RestauranteRepositorio();
		ArrayList<Restaurante> lista = repositorio.criarRestaurantes();
		ArrayList<Restaurante> disponiveis = new ArrayList<Restaurante>();

		// criacao de um combox com os restaurantes aptos para votacao no dia
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 13));
		comboBox.addItem("");
		for (Restaurante r : lista) {
			if (r.getVisitado() == false) {// condicao para aceitar apenas restaurantes nao escolhidos ainda na semana
				comboBox.addItem(r.getNome());
				disponiveis.add(r);
			}
		}
		comboBox.setBounds(204, 181, 204, 33);
		panel.add(comboBox);

		JLabel lblNewLabel = new JLabel("Selecione um Restaurante:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(26, 175, 186, 42);
		panel.add(lblNewLabel);

		JButton btnNewButton = new JButton("Votar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String nomerestaurante = comboBox.getSelectedItem().toString();
				VotacaoServicos v = new VotacaoServicos();
				ArrayList<Votacao> votacao = new ArrayList<Votacao>();
				votacao = v.novoVoto(nomerestaurante, disponiveis, numVotos);
				JOptionPane.showMessageDialog(null, "Voto Computado com Sucesso!");
				telaopcoes tela = new telaopcoes(votacao, nome);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(164, 248, 108, 33);
		panel.add(btnNewButton);

	}
}
