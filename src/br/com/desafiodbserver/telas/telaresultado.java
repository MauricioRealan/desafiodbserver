package br.com.desafiodbserver.telas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.repositorio.VotacaoRepositorio;
import br.com.desafiodbserver.servicos.VotacaoServicos;
import br.com.desafiodbserver.util.gerarArquivo;

import javax.swing.border.LineBorder;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//classe que monta a tela onde o resultado da votacao eh apresentado
public class telaresultado extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JButton button;
	private JButton btnVoltar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telaresultado frame = new telaresultado(null, "");
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public telaresultado(String[] resultados, String nome) {
		setTitle("- DBServer - Sele��o Restaurantes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 434, 308);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(96, 9, 230, 143);
		label.setIcon(new ImageIcon(telaescolha.class.getResource("/br/com/desafiodbserver/telas/dbserver.png")));
		panel.add(label);

		JLabel lblNewLabel = new JLabel("Restaurante");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(132, 143, 107, 26);
		panel.add(lblNewLabel);

		JLabel lblVotos = new JLabel("Votos");
		lblVotos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblVotos.setBounds(317, 143, 107, 26);
		panel.add(lblVotos);

		textField = new JTextField();
		textField.setBounds(45, 170, 247, 26);
		panel.add(textField);
		textField.setColumns(10);
		textField.setText(resultados[0]);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(293, 170, 86, 26);
		panel.add(textField_1);
		textField_1.setText(resultados[1]);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(45, 198, 247, 26);
		panel.add(textField_2);
		textField_2.setText(resultados[2]);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(293, 198, 86, 26);
		panel.add(textField_3);
		textField_3.setText(resultados[3]);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(45, 225, 247, 26);
		panel.add(textField_4);
		textField_4.setText(resultados[4]);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(293, 225, 86, 26);
		panel.add(textField_5);
		textField_5.setText(resultados[5]);

		button = new JButton("Log Out");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				telalogin tela = new telalogin(nome);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				setVisible(false);

			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 14));
		button.setBounds(66, 262, 134, 32);
		panel.add(button);

		btnVoltar = new JButton("Atualizar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				gerarArquivo arquivo = new gerarArquivo();
				ArrayList<Votacao> listavotos = arquivo.carregarResultados();
				VotacaoRepositorio votacaorep = new VotacaoRepositorio();
				ArrayList<Votacao> votos = votacaorep.alimentarVotacao(listavotos);
				arquivo.gravarResultados(votos);
				VotacaoServicos votacao = new VotacaoServicos();
				String[] lista = new String[6];
				lista = votacao.buscarResultado(votos);
				telaresultado tela = new telaresultado(lista, nome);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				setVisible(false);

			}
		});
		btnVoltar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnVoltar.setBounds(225, 262, 134, 32);
		panel.add(btnVoltar);
	}
}
