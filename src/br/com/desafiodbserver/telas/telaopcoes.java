package br.com.desafiodbserver.telas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.servicos.VotacaoServicos;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

//classe que monta a tela onde sao apresentadas as opcoes de acao pos voto para o usuario
public class telaopcoes extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telaopcoes frame = new telaopcoes(null, "");
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public telaopcoes(ArrayList<Votacao> listavotos, String nome) {
		setTitle("- DBServer - Sele��o Restaurantes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 434, 308);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(96, 9, 230, 143);
		label.setIcon(new ImageIcon(telaescolha.class.getResource("/br/com/desafiodbserver/telas/dbserver.png")));
		panel.add(label);

		JButton btnNewButton = new JButton("Conferir Resultado");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				VotacaoServicos votacao = new VotacaoServicos();
				String[] resultado = new String[6];
				resultado = votacao.buscarResultado(listavotos);
				telaresultado tela = new telaresultado(resultado, nome);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				setVisible(false);

			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(123, 169, 172, 42);
		panel.add(btnNewButton);

		JButton btnLogOut = new JButton("Log Out");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				telalogin tela = new telalogin(nome);
				tela.setLocationRelativeTo(null);
				tela.setVisible(true);
				setVisible(false);
			}
		});
		btnLogOut.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnLogOut.setBounds(123, 231, 172, 42);
		panel.add(btnLogOut);
	}
}
