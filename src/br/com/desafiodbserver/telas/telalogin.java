package br.com.desafiodbserver.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.servicos.FuncionarioServicos;
import br.com.desafiodbserver.servicos.VotacaoServicos;
import br.com.desafiodbserver.util.gerarArquivo;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

//classe que monta a tela onde eh realizado o login no sistema
public class telalogin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telalogin frame = new telalogin("");
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public telalogin(String nome) {
		setTitle("- DBServer - Sele��o Restaurantes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 434, 308);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Usu\u00E1rio: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(96, 164, 72, 25);
		panel.add(lblNewLabel);

		JLabel label = new JLabel("");
		label.setBounds(96, 9, 230, 143);
		label.setIcon(new ImageIcon(telalogin.class.getResource("/br/com/desafiodbserver/telas/dbserver.png")));
		panel.add(label);

		JLabel lblSenha = new JLabel("Senha: ");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha.setBounds(96, 208, 72, 25);
		panel.add(lblSenha);

		textField = new JTextField();
		textField.setBounds(165, 163, 165, 25);
		panel.add(textField);
		textField.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(165, 207, 165, 25);
		panel.add(passwordField);

		JButton btnNewButton = new JButton("Entrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String usuario = textField.getText();
				String senha = new String(passwordField.getPassword());

				FuncionarioServicos servico = new FuncionarioServicos();
				Boolean resultado = servico.checarUsuario(usuario, senha);

				// condicao quando o usuario ou senha nao sao encontrados
				if (resultado == false) {
					JOptionPane.showMessageDialog(null, "Erro! Usu�rio ou Senha Inv�lidos.");
					textField.setText("");
					passwordField.setText("");
				}

				// condicao quando eh realizado o primero voto no sistema
				if (nome == "" && resultado == true) {
					JOptionPane.showMessageDialog(null, "Acesso com Sucesso!");
					gerarArquivo arquivo = new gerarArquivo();
					arquivo.guardarVotantes(usuario);
					telaescolha tela = new telaescolha(usuario, 1);
					tela.setLocationRelativeTo(null);
					tela.setVisible(true);
					setVisible(false);
				}

				// condicao para os demais votos realizados no sistema
				if (nome != "" && resultado == true) {

					VotacaoServicos votos = new VotacaoServicos();
					Boolean confirmacao = votos.conferirVontantes(usuario);

					// condicao para um novo acesso de um usuario especifico
					if (confirmacao == true) {
						JOptionPane.showMessageDialog(null,
								"Acesso com Sucesso! Voc� J� Votou Hoje, Confira o Resultado.");
						gerarArquivo arquivo = new gerarArquivo();
						ArrayList<Votacao> listavotos = arquivo.carregarResultados();
						VotacaoServicos votacao = new VotacaoServicos();
						String[] lista = new String[6];
						lista = votacao.buscarResultado(listavotos);
						telaresultado tela = new telaresultado(lista, usuario);
						tela.setLocationRelativeTo(null);
						tela.setVisible(true);
						setVisible(false);
					}
					// condicao para o primeiro acesso de um usuario especifico
					if (confirmacao == false) {
						JOptionPane.showMessageDialog(null, "Acesso com Sucesso!");
						gerarArquivo arquivo = new gerarArquivo();
						arquivo.guardarVotantes(usuario);
						telaescolha tela = new telaescolha(usuario, 2);
						tela.setLocationRelativeTo(null);
						tela.setVisible(true);
						setVisible(false);
					}
				}

			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(165, 250, 110, 32);
		panel.add(btnNewButton);
	}
}
