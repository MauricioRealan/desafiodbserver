package br.com.desafiodbserver.servicos;

import java.util.ArrayList;

import br.com.desafiodbserver.dominio.Funcionario;
import br.com.desafiodbserver.repositorio.FuncionarioRepositorio;

// classe que executa servicos referentes aos dados de funcionarios
public class FuncionarioServicos {

	private Boolean resultado = false;

	// metodo que avalia se o usuario e senha passados correspondem na base
	public Boolean checarUsuario(String usuario, String senha) {

		ArrayList<Funcionario> lista = new ArrayList<Funcionario>();

		FuncionarioRepositorio repositorio = new FuncionarioRepositorio();

		lista = repositorio.criarUsuarios();

		for (Funcionario f : lista) {
			if (usuario.equals(f.getUsuario()) && senha.equals(f.getSenha())) {
				resultado = true; // "true" indica que o usuario e senha conferem
			}
		}

		return resultado;
	}

}
