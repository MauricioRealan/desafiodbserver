package br.com.desafiodbserver.servicos;

import java.util.ArrayList;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;
import br.com.desafiodbserver.repositorio.VotacaoRepositorio;
import br.com.desafiodbserver.util.gerarArquivo;

//classe que executa servicos referentes aos dados da votacao
public class VotacaoServicos {

	VotacaoRepositorio v = new VotacaoRepositorio();
	ArrayList<Votacao> votacao = new ArrayList<Votacao>();

	// metodo responsavel por computar os novos votos de usarios que entraram no
	// sistema
	public ArrayList<Votacao> novoVoto(String restaurante, ArrayList<Restaurante> disponiveis, int numVotos) {

		// condicao quando eh o primero voto realizado atraves do sistema
		if (numVotos == 1) {
			votacao = v.criarVotacao(disponiveis);
		}

		// condicao quando o primeiro voto ja foi realizado atraves do sistema
		if (numVotos == 2) {
			gerarArquivo arq = new gerarArquivo();
			votacao = arq.carregarResultados();
		}

		for (Votacao votos : votacao) {
			if (votos.getRestaurante().getNome().equals(restaurante)) {
				votos.setVotos(votos.getVotos() + 1);
			}
		}

		ArrayList<Votacao> aumentarvotacao = v.alimentarVotacao(votacao);

		gerarArquivo arquivo = new gerarArquivo();
		arquivo.gravarResultados(aumentarvotacao);// grava os novos votos em um arquivo temporario

		return aumentarvotacao;
	}

	// metodo que retorna os votos dos tres restaurantes mais votados
	public String[] buscarResultado(ArrayList<Votacao> votacaofinal) {

		String[] resultado = new String[6];
		int temp = 1, temp2 = 1, temp3 = 1;

		for (Votacao votos : votacaofinal) {
			if (votos.getVotos() >= temp) {
				temp2 = temp;
				temp = votos.getVotos();
			}
			if (votos.getVotos() < temp && votos.getVotos() >= temp2 && votos.getVotos() > temp3) {
				temp3 = temp2;
				temp2 = votos.getVotos();
			}
			if (votos.getVotos() < temp && votos.getVotos() < temp2 && votos.getVotos() > temp3) {
				temp3 = votos.getVotos();
			}
		}

		for (Votacao votos : votacaofinal) {
			if (votos.getVotos() == temp) {
				resultado[0] = votos.getRestaurante().getNome();
				resultado[1] = Integer.toString(votos.getVotos());
				temp = 1;
			}
			if (votos.getVotos() == temp2) {
				resultado[2] = votos.getRestaurante().getNome();
				resultado[3] = Integer.toString(votos.getVotos());
				temp2 = 1;
			}
			if (votos.getVotos() == temp3) {
				resultado[4] = votos.getRestaurante().getNome();
				resultado[5] = Integer.toString(votos.getVotos());
				temp3 = 1;
			}
		}

		return resultado;
	}

	// metodo que confere se o usuario que esta acessando o sistema ja fez seu voto
	public Boolean conferirVontantes(String nome) {

		Boolean utilizado = false;

		gerarArquivo arquivo = new gerarArquivo();
		String[] votantes = arquivo.carregarVotantes();

		for (int i = 0; i < votantes.length; i++) {
			if (nome.equals(votantes[i])) {
				utilizado = true;
			}
		}

		return utilizado;
	}

}
