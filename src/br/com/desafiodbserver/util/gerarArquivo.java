package br.com.desafiodbserver.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import br.com.desafiodbserver.dominio.Restaurante;
import br.com.desafiodbserver.dominio.Votacao;

public class gerarArquivo {

	public void gravarResultados(ArrayList<Votacao> votacao) {

		try {
			FileWriter writer = new FileWriter("votos.txt", false);
			for (Votacao v : votacao) {
				writer.write(v.getRestaurante().getNome());
				writer.write(":");
				writer.write(String.valueOf(v.getRestaurante().getVisitado()));
				writer.write(":");
				writer.write(String.valueOf(v.getVotos()));
				writer.write(":");
				writer.write("certo");
				writer.write("\r\n");
			}
			// writer.write("\r");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<Votacao> carregarResultados() {

		ArrayList<Votacao> resultado = new ArrayList<Votacao>();

		try {

			Scanner scanner = new Scanner(new FileReader("votos.txt")).useDelimiter("\\n");

			while (scanner.hasNext()) {

				String linha = scanner.next();
				String[] temp = linha.split(":");

				Votacao votacao = new Votacao();
				Restaurante restaurante = new Restaurante();
				restaurante.setNome(temp[0]);
				restaurante.setVisitado(Boolean.parseBoolean(temp[1]));
				votacao.setRestaurante(restaurante);
				int numvoto = Integer.valueOf(temp[2]);
				votacao.setVotos(numvoto);
				resultado.add(votacao);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return resultado;

	}

	public void guardarVotantes(String funcionario) {

		try {
			FileWriter writer = new FileWriter("votantes.txt", true);

			writer.write(funcionario);

			writer.write("\r\n");

			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String[] carregarVotantes() {

		String[] votantes = new String[10];
		int i = 0;

		try {

			Scanner scanner = new Scanner(new FileReader("votantes.txt")).useDelimiter("\\n");

			while (scanner.hasNext()) {

				String linha = scanner.next();
				votantes[i] = linha.trim();
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return votantes;
	}

}
